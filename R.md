# R

R es un lenguaje de análisis de datos y generación de gráficos, está diseñado
para la fácil manipulación de datos. Es sucesor del lenguaje S.

En R, no se recomienda usar ciclos como `for`, `while`, etc. Ya que es mucho más
eficiente vectorizar nuestras operaciones.

R cuenta con una gran adopción por la comunidad científica, por lo que muchas técnicas
nuevas aparecen primero en este ambiente que en otros lenguajes.
